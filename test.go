package main

import (
	"fmt"
	"os"
	"path/filepath"
)

func main() {
	path, err := os.Executable()

	if err == nil {
		fmt.Println(filepath.Base(path))
	}
}
