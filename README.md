# Symlink Test

1. Build `test.go`: `go build test.go`
1. Run `test`: `./test`. Should print out `test`.
1. Run `symlink`: `./symlink`. Should print out `symlink`.
